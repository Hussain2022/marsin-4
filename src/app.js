// __dirname stands for diractory name which contains the path to the directory the current script lives in
// __filename which contains the complete path from the root of the hard drive all the way to the src folder
// For more info go to https://nodejs.org/dist/latest-v13.x/docs/api/path.html
// npmjs.com/package/handlebars
// expressjs.com/api.html

const path = require('path')
const express = require('express')
const hbs = require('hbs')

const app = express()

// Define paths for Express config
const publicDirPath = path.join(__dirname, '../public')
const viewsPath = path.join(__dirname, '../templates/views')
const partialsPath = path.join(__dirname, '../templates/partials')

// Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views', viewsPath)
hbs.registerPartials(partialsPath)

// Setup static directory to serve
app.use(express.static(path.join(publicDirPath)))

app.get('/', (req, res) => {
    res.render('index', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/gaming', (req, res) => {
    res.render('gaming', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/music', (req, res) => {
    res.render('music', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/sports', (req, res) => {
    res.render('sports', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/news', (req, res) => {
    res.render('news', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/fashion', (req, res) => {
    res.render('fashion', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/technology', (req, res) => {
    res.render('technology', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/flexible', (req, res) => {
    res.render('flexible', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/moons', (req, res) => {
    res.render('moons', {
        title: 'MarsIN',
        name: 'Team marsin'
    })
})
app.get('/profile', (req, res) => {
    res.render('profile', {
        title: 'MarsIN'
    })
})

app.get('*', (req, res) => {
    res.render('404', {
        title: 'MarsIN',
        name: 'Team marsin',
        error: '404',
        message: 'This page doesn\'t exit'
    })
})

app.listen(3000, () => {
    console.log('Server is up on port 3000.')
})